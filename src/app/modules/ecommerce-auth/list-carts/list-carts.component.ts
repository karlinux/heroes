import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EcommerceGuestService } from '../../ecommerce-guest/_service/ecommerce-guest.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-list-carts',
  templateUrl: './list-carts.component.html',
  styleUrls: ['./list-carts.component.scss']
})
export class ListCartsComponent {

  url = environment.URL_SERVICIOS;

  listCart:any = [];
  totalCart: any = 0;
  constructor(
    public router: Router,
    public cartService: EcommerceGuestService,

  ){

  }

  ngOnInit(){
    this.cargarDatos();
  }

  dec(cart: any, decInc: number){
    if(decInc == 1){

      if(cart.cantidad - 1 == 0  ){
        return;
      }else{
        cart.cantidad = cart.cantidad - 1
        cart.subtotal = cart.precio_dolar * cart.cantidad;
        cart.total = cart.precio_dolar * cart.cantidad;
      }
    }else{
      cart.cantidad = Number(cart.cantidad) + 1
      cart.total = cart.precio_dolar * cart.cantidad;
    }

    let data = {
      "_id": cart._id,
      "cantidad": cart.cantidad,
      "subtotal": cart.subtotal,
      "total": cart.total,
      "tabla": "car_shop"
    }

    this.cartService.updateCarrito( data ).subscribe( (resp: any) => {
      console.log(resp)
      this.cartService.changeCart( cart );
      this.cargarDatos();
    })
  }

  removeCart(cart: any){
    let data = {
      "id": cart._id,
      "tabla": "car_shop"
    }

 this.cartService.deleteCarrito( data ).subscribe( (resp: any) => {
      console.log(resp)
      this.cartService.removeItemCart( cart );
    })
    this.cargarDatos();
  }

  cargarDatos(){
    this.cartService.currentDataCart$.subscribe((resp: any) =>{
      console.log( resp )
        this.listCart = resp;
        this.totalCart = this.listCart.reduce((sum:any,item:any) => Number(sum) + Number(item.total), 0);
     })
  }

}
