import { Component, OnInit } from '@angular/core';
import { EcommerceAuthService } from '../_service/ecommerce-auth.service';
import { JwtService } from '../../auth-profile/_services/jwt.service';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile-client',
  templateUrl: './profile-client.component.html',
  styleUrls: ['./profile-client.component.css']
})
export class ProfileClientComponent implements OnInit {

  nombre: String = '';
  paterno: String = '';
  materno: String = '';
  fecha: any = '';
  viewOrder: boolean = false;
  listClient: any = []
  orders: any = []
  orderId: any = []
  url = environment.URL_SERVICIOS;
  listAdressClient: any = []
  listCountrys: any = []
  boton: string = "Register"
  grupoForm: FormGroup;

  constructor(
    public ecommerceAuthService: EcommerceAuthService,
    private _jwtService: JwtService,
    private fb: FormBuilder,
  ){
    this.formulario(); }

  ngOnInit(): void {
    this.profileClient();
    this.getOrders();
    this.nombre = this._jwtService.decodeToken(this.getToken()).profile.nombre
    this.paterno = this._jwtService.decodeToken(this.getToken()).profile.paterno
    this.materno = this._jwtService.decodeToken(this.getToken()).profile.materno
    this.fecha = this._jwtService.decodeToken(this.getToken()).profile.fecha
  }

  profileClient(){
    this.ecommerceAuthService.getProfileClient().subscribe(resp =>{
      this.listClient = resp;
    })
  }

  formulario(){
    this.grupoForm = this.fb.group({
      id:[],
      name:['', Validators.required],
      lastname:['', Validators.required],
      pais:['', Validators.required],
      address:['', Validators.required],
      colonia:[''],
      region:[''],
      city:['', Validators.required],
      phone:['', Validators.required],
      email:['', Validators.required],
      notes:[''],
      tabla:['adress_client', Validators.required],
    });
  }
  getOrders(){
    this.ecommerceAuthService.getOrdersClient().subscribe(resp =>{
      this.orders = resp;
      console.log(resp)
    })
    this.ecommerceAuthService.listAddressClient().subscribe(resp =>{
      this.listAdressClient = resp;
    })
    let data = {
      "tabla": "countries"
    }
    this.ecommerceAuthService.getCatalogo( data ).subscribe((resp: any) =>{
      this.listCountrys = resp;
     })
  }

  getToken(): any{

    if (typeof localStorage !== 'undefined') {
        return localStorage.getItem('token');
    }
  }
  logout(){
    localStorage.clear();
    location.reload();
  }
  view( order: any ){

      if(order){
        this.obtenerOrden( order );
      }

      this.viewOrder = this.viewOrder ? this.viewOrder = false : this.viewOrder = true;

  }

  selecDireccion(direccion: number){

    this.grupoForm.controls['id'].setValue(this.listAdressClient[direccion].id);
    this.grupoForm.controls['name'].setValue(this.listAdressClient[direccion].name);
    this.grupoForm.controls['lastname'].setValue(this.listAdressClient[direccion].lastname);
    this.grupoForm.controls['pais'].setValue(this.listAdressClient[direccion].pais);
    this.grupoForm.controls['address'].setValue(this.listAdressClient[direccion].address);
    this.grupoForm.controls['colonia'].setValue(this.listAdressClient[direccion].colonia);
    this.grupoForm.controls['region'].setValue(this.listAdressClient[direccion].region);
    this.grupoForm.controls['city'].setValue(this.listAdressClient[direccion].city);
    this.grupoForm.controls['phone'].setValue(this.listAdressClient[direccion].phone);
    this.grupoForm.controls['email'].setValue(this.listAdressClient[direccion].email);
    this.grupoForm.controls['notes'].setValue(this.listAdressClient[direccion].notes);
    this.boton = "Update";

  }

  registrarDireccion(){
    if (this.grupoForm.invalid) {
      this.grupoForm.markAllAsTouched();
      //TODO alerta
      // Swal.fire({
        //   icon: 'error',
        //   title: 'Error',
        //   text: 'Error al guardar los datos'
        // })
        console.log("ERROR")
        this.formulario();
        return;
      }
    this.ecommerceAuthService.registrarDireccion(this.grupoForm.value ).subscribe((resp:any) =>{

      //Todo Alerta
      if(resp.estatus){
        this.getOrders();
        this.formulario();
      }
      this.boton = "Register"
    })
  }

  obtenerOrden(  order: any  ){
    this.ecommerceAuthService.getOrder(  order ).subscribe(resp =>{
      this.orderId = resp;
      console.log(resp)
    })
  }

  clean(){
    this.formulario();
    this.boton = "Register";
  }
}

