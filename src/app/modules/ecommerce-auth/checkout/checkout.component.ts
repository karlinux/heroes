import { Component, ElementRef, ViewChild } from '@angular/core';
import { EcommerceAuthService } from '../_service/ecommerce-auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EcommerceGuestService } from '../../ecommerce-guest/_service/ecommerce-guest.service';

declare var paypal:any;
declare function alertDanger([]): any;

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})

export class CheckoutComponent {

  @ViewChild('paypal',{static: true}) paypalElement?: ElementRef;
  listAdressClient: any = []
  listCountrys: any = []
  tipopago: boolean = true

  listCart:any = [];
  totalCart: any = 0;

  grupoForm: FormGroup;
  grupoFormCheckOut: FormGroup;
  boton: string = "Register"
  constructor(
    public ecommerceAuthService: EcommerceAuthService,
    public cartService: EcommerceGuestService,
    private fb: FormBuilder,
  ){
    this.formulario();
  }

  ngOnInit(){

    this.cargarDatos();
    if(this.tipopago){
      this.paypal();
    }
  }

  paypal(){

    paypal.Buttons({
      // optional styling for buttons
      // https://developer.paypal.com/docs/checkout/standard/customize/buttons-style-guide/
      style: {
        color: "gold",
        shape: "rect",
        layout: "vertical"
      },

      // set up the transaction
      createOrder: (data:any, actions:any) => {
          // pass in any options from the v2 orders create call:
          // https://developer.paypal.com/api/orders/v2/#orders-create-request-body
          if(this.listCart.length == 0){
            //TODO agregar alerta
            alertDanger("AGREGUE PRODUCTOS A SU CARRITO DE COMPRAS");
            console.log("Agregue productos a su carrito de compras")
            return;
          }

          console.log(this.listCart);

          if (this.grupoForm.invalid){
            //TODO agregar alerta
            alertDanger("SELECCIONE UNA DIRECCIÓN DE ENVÍO");
            console.log("Seleccione una dirección de envío")
            return;
          }
          const createOrderPayload = {
            purchase_units: [
              {
                amount: {
                    description: "COMPRAR POR EL ECOMMERCE",
                    value: this.totalCart
                }
              }
            ]
          };

          return actions.order.create(createOrderPayload);
      },

      // finalize the transaction
      onApprove: async (data:any, actions:any) => {

          let Order = await actions.order.capture();

          this.envio(Order)
  // Order.purchase_units[0].payments.captures[0].id

      },

      // handle unrecoverable errors
      onError: (err:any) => {
          console.error('An error prevented the buyer from checking out with PayPal');
      }
    }).render(this.paypalElement?.nativeElement);

  }

  envio( Order: any){

    let sale = {
      "user_id": 'item.id',
      "currency_payment": 'USD',
      "method_payment": "PAYPAL",
      "n_transaccion": Order.purchase_units[0].payments.captures[0].id,
      "total": this.totalCart,
      "currency_total": this.totalCart,
      "price_dolar": this.totalCart,
      "address": this.grupoForm.value
    }
        // return actions.order.capture().then(captureOrderHandler);
        this.ecommerceAuthService.registrarCompra( sale ).subscribe( (resp: any) =>{
          console.log(resp)
          if(resp.status){

            this.ecommerceAuthService.sendMail( resp ).subscribe( (resp: any) =>{
                console.log(resp)
            })
          }

        })

  }
  formulario(){
    this.grupoForm = this.fb.group({
      id:[],
      name:['', Validators.required],
      lastname:['', Validators.required],
      pais:['', Validators.required],
      address:['', Validators.required],
      colonia:[''],
      region:[''],
      city:['', Validators.required],
      phone:['', Validators.required],
      email:['', Validators.required],
      notes:[''],
      tabla:['adress_client', Validators.required],
    });
    this.grupoFormCheckOut = this.fb.group({
      id:['',Validators.required],
      name:['', Validators.required],
      lastname:['', Validators.required],
      pais:['', Validators.required],
      address:['', Validators.required],
      colonia:[''],
      region:[''],
      city:['', Validators.required],
      phone:['', Validators.required],
      email:['', Validators.required],
      notes:[''],
      tabla:['adress_client', Validators.required],
    });
  }

  cargarDatos(){
    let data = {
      "tabla": "countries"
    }
    this.ecommerceAuthService.getCatalogo( data ).subscribe((resp: any) =>{
      this.listCountrys = resp;
     })
     this.ecommerceAuthService.listAddressClient().subscribe(resp =>{
      this.listAdressClient = resp;
    })

    /************************ Cargar datos carrito de compras  ********************/
    this.cartService.currentDataCart$.subscribe((resp: any) =>{
      this.listCart = resp;
      this.totalCart = this.listCart.reduce((sum:any,item:any) => Number(sum) + Number(item.total), 0);
   })
  }

  registrarDireccion(){
    if (this.grupoForm.invalid) {
      this.grupoForm.markAllAsTouched();
      //TODO alerta
      // Swal.fire({
        //   icon: 'error',
        //   title: 'Error',
        //   text: 'Error al guardar los datos'
        // })
        console.log("ERROR")
        this.formulario();
        return;
      }
    this.ecommerceAuthService.registrarDireccion(this.grupoForm.value ).subscribe((resp:any) =>{

      //Todo Alerta
      if(resp.estatus){
        this.cargarDatos()
        this.formulario();
      }
      this.boton = "Register"
    })
  }

  selecDireccion(direccion: number){

    this.grupoForm.controls['id'].setValue(this.listAdressClient[direccion].id);
    this.grupoForm.controls['name'].setValue(this.listAdressClient[direccion].name);
    this.grupoForm.controls['lastname'].setValue(this.listAdressClient[direccion].lastname);
    this.grupoForm.controls['pais'].setValue(this.listAdressClient[direccion].pais);
    this.grupoForm.controls['address'].setValue(this.listAdressClient[direccion].address);
    this.grupoForm.controls['colonia'].setValue(this.listAdressClient[direccion].colonia);
    this.grupoForm.controls['region'].setValue(this.listAdressClient[direccion].region);
    this.grupoForm.controls['city'].setValue(this.listAdressClient[direccion].city);
    this.grupoForm.controls['phone'].setValue(this.listAdressClient[direccion].phone);
    this.grupoForm.controls['email'].setValue(this.listAdressClient[direccion].email);
    this.grupoForm.controls['notes'].setValue(this.listAdressClient[direccion].notes);

    this.grupoFormCheckOut.controls['id'].setValue(this.listAdressClient[direccion].id);
    this.grupoFormCheckOut.controls['name'].setValue(this.listAdressClient[direccion].name);
    this.grupoFormCheckOut.controls['lastname'].setValue(this.listAdressClient[direccion].lastname);
    this.grupoFormCheckOut.controls['pais'].setValue(this.listAdressClient[direccion].pais);
    this.grupoFormCheckOut.controls['address'].setValue(this.listAdressClient[direccion].address);
    this.grupoFormCheckOut.controls['colonia'].setValue(this.listAdressClient[direccion].colonia);
    this.grupoFormCheckOut.controls['region'].setValue(this.listAdressClient[direccion].region);
    this.grupoFormCheckOut.controls['city'].setValue(this.listAdressClient[direccion].city);
    this.grupoFormCheckOut.controls['phone'].setValue(this.listAdressClient[direccion].phone);
    this.grupoFormCheckOut.controls['email'].setValue(this.listAdressClient[direccion].email);
    this.grupoFormCheckOut.controls['notes'].setValue(this.listAdressClient[direccion].notes);
    this.boton = "Update";

  }
  clean(){
    this.formulario();
    this.boton = "Register";
  }
}
