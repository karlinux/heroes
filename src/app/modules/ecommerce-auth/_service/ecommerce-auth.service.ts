import { Injectable } from '@angular/core';
import { AuthService } from '../../auth-profile/_services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EcommerceAuthService {
  url = environment.URL_SERVICIOS;

  constructor(
    public _authService: AuthService,
    public http: HttpClient
  ) { }

  listAddressClient(){
    let token:string  = this._authService.getToken();
    let headers= null;;
    if(token){
      headers = new HttpHeaders({'token': token})
    }else{
      headers = new HttpHeaders({'token': ''})

    }
    let URL = this.url + "getAddress.php"
    return this.http.post(URL, "", {headers: headers});
  }

  getCatalogo(data: any){
    let URL = this.url + "getCatalogos.php"
    return this.http.post(URL, data);
  }

  registrarDireccion(address: any){
    let token:string  = this._authService.getToken();
    let headers= null;;
    if(token){
      headers = new HttpHeaders({'token': token})
    }else{
      headers = new HttpHeaders({'token': ''})
    }
    let URL = this.url + "insertarAddres.php"
    return this.http.post(URL, address, {headers: headers});
  }

    getProfileClient(){
    let token:string  = this._authService.getToken();
    let headers= null;;
    if(token){
      headers = new HttpHeaders({'token': token})
    }else{
      headers = new HttpHeaders({'token': ''})

    }
    let URL = this.url + "getAddress.php"
    return this.http.post(URL, "", {headers: headers});
  }

  getOrdersClient(){
    let token:string  = this._authService.getToken();
    let headers= null;;
    if(token){
      headers = new HttpHeaders({'token': token})
    }else{
      headers = new HttpHeaders({'token': ''})

    }
    let URL = this.url + "getOrders.php"
    return this.http.post(URL, "", {headers: headers});
  }

  getOrder( datos: any ){
    let token:string  = this._authService.getToken();
    let headers= null;;
    if(token){
      headers = new HttpHeaders({'token': token})
    }else{
      headers = new HttpHeaders({'token': ''})

    }
    let URL = this.url + "getOrderById.php"
    return this.http.post(URL, datos, {headers: headers});
  }

    registrarCompra( data: any){
      let token:string  = this._authService.getToken();
      let headers= null;;
      if(token){
        headers = new HttpHeaders({'token': token})
      }else{
        headers = new HttpHeaders({'token': ''})

      }
      let URL = this.url + "insertarVenta.php"
      return this.http.post(URL, data, {headers: headers});

  }

  sendMail(data: any){
    let token:string  = this._authService.getToken();
      let headers= null;;
      if(token){
        headers = new HttpHeaders({'token': token})
      }else{
        headers = new HttpHeaders({'token': ''})

      }
      let URL = this.url + "checkout.php"
      return this.http.post(URL, data, {headers: headers});
  }

}
