import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';

declare function alertSuccess([]): any;
declare function alertDanger([]): any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  grupoForm: FormGroup;

  constructor(
    public authService: AuthService,
    private fb: FormBuilder,
    private router: Router
  ){
    this.formulario();
  }
  formulario(){
    this.grupoForm = this.fb.group({
      email:['vicion@hotmail.com', Validators.required],
      password:['123456', Validators.required],
      repassword:['123456'],
      name:['Carlos', Validators.required],
      lastname:['Cruz', Validators.required],
      telephone:['2281111199'],
      birthday:[''],
    });
  }

  save(){
    if (this.grupoForm.invalid) {
      this.grupoForm.markAllAsTouched();
      alertDanger("ERROR AL REGISTRARSE TODOS LOS DATOS DEBEN SER VÁLIDOS");

      return;
    }
    if(this.grupoForm.value.password != this.grupoForm.value.repassword){
      alertDanger("LAS CONTRASEÑAS NO SON IGUALES");
      return;
    }

    this.authService.regsitro(this.grupoForm.value ).subscribe((resp:any) =>{
      console.log( resp )
      if(resp.estado){
        //this.router.navigate(["/"])
        alertSuccess("SESIÓN INICIADA CORRECTAMENTE ");
      }else{
        alertDanger("ERROR AL INICIAR SESIÓN " + resp.mensaje );

      }
    })
  }
}
