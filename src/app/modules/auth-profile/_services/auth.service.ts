import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { URL_SERVICIOS } from '../../../config/config';
import { Observable, of} from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _auth: any | undefined;

  constructor(
    private http: HttpClient,
    private router: Router,
    private _jwtService: JwtService
  ) { }

  login( login: any ){

    let URL = URL_SERVICIOS + "log.php"
    return this.http.post(URL, login).pipe(
      map((resp:any)=>{
       // console.log(resp)
        if(resp.status){
           return this.localStorageSave(resp)
        }else{
          return false
        }
      }),
      catchError((error:any) => {
        console.log(error);
        return error;
      })
    )

  }

  getTok(token: String): Observable<any>  {
    return this.http.post<any>(`${URL_SERVICIOS}token.php`, localStorage.getItem('token'));
  }

  localStorageSave(resp:any){
    localStorage.setItem("token", resp.token);
    return true;
  }

  regsitro(data:any){
    let URL = URL_SERVICIOS + "register.php"
    return this.http.post(URL, data);
  }

  getEmail() {
    return `${this._jwtService.decodeToken(this.getToken()).profile.email}`;
  }
  logout(){
    localStorage.removeItem("token")
    this.router.navigate(["auth/login"])
  }
  getToken(): any{

    if (typeof localStorage !== 'undefined') {
        return localStorage.getItem('token');
    }
  }

  verifica() {
    let tr: boolean = true;
    if(!sessionStorage.getItem('token')){
      return of(false);
    }
      if (typeof localStorage !== 'undefined') {
        if (localStorage.getItem('token')) {
          var token = localStorage.getItem('token');
          this.getTok(token!).subscribe( resp => {
            console.log(resp)
            if(resp!=null){
              tr = true;
              // localStorage.clear();
            }else{
              tr = false;
            }
          })
        }else{
          tr = false;
        }
      }
    return tr

  }


  IsAutenticated(){
    let tr = true;

        if (localStorage.getItem('token')) {
          var token = localStorage.getItem('token');

          this.getTok(token!).subscribe( resp => {
            localStorage.setItem('token', resp.token);
            if(resp.token === ''){
              this.router.navigate(["/"])
            }
            if(resp != null){
              if(resp.token == ''){
                tr = false;
              }else{
                tr = true;
              }
              // localStorage.clear();
            }else{
              tr = false;
            }
          })
        }else{
          tr = false;
        }
    return tr
  }
}
