import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

declare function alertSuccess([]): any;
declare function alertDanger([]): any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  grupoForm: FormGroup;
  email:string = "";
  password:string = "";

  constructor(
    public authService: AuthService,
    private fb: FormBuilder,
    private router: Router
  ){
    this.formulario();
  }

  ngOnInit():void{

  }

  campoEsValido(campo: string) {
    return (
      this.grupoForm.controls[campo].errors && this.grupoForm.controls[campo].touched
    );
  }

  formulario(){
    this.grupoForm = this.fb.group({
      email:['', Validators.required],
      password:['', Validators.required],
    });
  }
  login(){

    if (this.grupoForm.invalid) {
      this.grupoForm.markAllAsTouched();
      // Swal.fire({
      //   icon: 'error',
      //   title: 'Error',
      //   text: 'Error al guardar los datos'
      // })
      return;
    }

    this.authService.login(this.grupoForm.value ).subscribe((resp:any) =>{
      if(resp){
        this.router.navigate(["/"])
        alertSuccess("SESIÓN INICIADA CORRECTAMENTE");
      }else{
        alertDanger("ERROR AL INICIAR SESIÓN");

      }
    })
  }
}
