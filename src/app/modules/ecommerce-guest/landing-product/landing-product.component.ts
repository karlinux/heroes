import { Component, OnInit } from '@angular/core';
import { EcommerceGuestService } from '../_service/ecommerce-guest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../environments/environment';

declare var $: any;
declare function LandingProductDetail(): any;
declare function alertWarning([]): any;
declare function alertDanger([]): any;
declare function alertSuccess([]): any;

@Component({
  selector: 'app-landing-product',
  templateUrl: './landing-product.component.html',
  styleUrls: ['./landing-product.component.scss']
})
export class LandingProductComponent implements OnInit{

  slug: any = null;
  product_selected: any = null;

  url = environment.URL_SERVICIOS;

  constructor(
    public ecommerce_guest: EcommerceGuestService,
    public router: Router,
    public activateRouter: ActivatedRoute,
  ){

  }

  ngOnInit(){
    this.activateRouter.params.subscribe( (resp:any) => {
      this.slug = resp["slug"]
      this.ecommerce_guest.getProducto(this.slug).subscribe( (resp:any) =>{
        this.product_selected = resp;
        setTimeout( () =>{
          LandingProductDetail();
        },50)
      })
    })
  }

  cargarCategoria( id_categoria:any ){
    this.ecommerce_guest.getProductoCategoría(id_categoria).subscribe( (resp:any) =>{

    })
  }

  addCart( producto: any ){

    let cantidad = $("#qty-cart").val()
    console.log(this.ecommerce_guest._authService.IsAutenticated())
    if(!this.ecommerce_guest._authService.IsAutenticated()){
      this.router.navigateByUrl('/auth/login');

      alertDanger("NECESITA INICIAR SESIÓN ANTES DE AGREGAR AL CARRITO DE COMPRAS");

      return;
    }
    if( cantidad == 0){
      alert("NECESITA AGREGAR UNA CANTIDAD MAYOR A 0 DEL PRODUCTO PARA EL CARRITO")
      return;
    }

    let data = {
      "product_id": this.product_selected.id,
      "type_discount": null,
      "discount": 0,
      "cantidad": cantidad,
      "variedad_id": 0,
      "code_cupon": null,
      "code_discount": null,
      "price_unitario": this.product_selected.precio_dolar,
      "subtotal": this.product_selected.precio_dolar * cantidad,
      "total": this.product_selected.precio_dolar * cantidad,
      "tabla": "car_shop",
    }

    console.log(data)
    this.ecommerce_guest.registerCart( data ).subscribe( (resp: any) =>{
      //localStorage.clear
      console.log(resp)
      if(resp){
        if(!resp.token){
          alertDanger("NECESITA INICIAR SESIÓN ANTES DE AGREGAR AL CARRITO DE COMPRAS");
          this.router.navigateByUrl('/auth/login');
        }else{
          this.ecommerce_guest.changeCart(resp.cart);
        }
      }else{
        alertDanger("NECESITA INICIAR SESIÓN ANTES DE AGREGAR AL CARRITO DE COMPRAS");
        this.router.navigateByUrl('/auth/login');
      }


    });
  }
}
