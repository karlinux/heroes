import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from '../../auth-profile/_services/auth.service';
import { URL_SERVICIOS } from '../../../config/config';

@Injectable({
  providedIn: 'root'
})
export class EcommerceGuestService {

  url = environment.URL_SERVICIOS;

  constructor(
    private http: HttpClient,
    public _authService: AuthService
  ) { }

  getProducto( datos: any ): Observable<any[]> {
    return this.http.get<any>(`${this.url}getProducto.php?slug=${datos}` );
  }
  getProductoCategoría( id: any ): Observable<any[]> {
    return this.http.get<any>(`${this.url}getProductosCategoria.php?id=${id}` );
  }

  //Carrito de compras
  public cart = new BehaviorSubject<Array<any>>([]);
  public currentDataCart$ = this.cart.asObservable();

  changeCart( DATACART: any ){
    let listCart = this.cart.getValue();
    let INDEX = listCart.findIndex( (item: any) => item._id == DATACART._id);
    if( INDEX != -1){
      listCart[ INDEX ] = DATACART;
    }else{
      listCart.unshift(DATACART);
    }
    this.cart.next( listCart );
  }
  resetCart(){
    let listCart: any = [];
    this.cart.next(listCart);
  }

  removeItemCart( DATACART: any ){
    console.log(DATACART)
    let listCart = this.cart.getValue();
    let INDEX = listCart.findIndex( (item: any) => item._id == DATACART._id);
    if( INDEX != -1){
      listCart.splice( INDEX, 1 );
    }
    this.cart.next(listCart);
  }

  registerCart( data: any ){
    let token:string  = this._authService.getToken();
    let headers= null;;
    if(token){
      headers = new HttpHeaders({'token': token})
    }else{
      headers = new HttpHeaders({'token': ''})

    }
      let URL = this.url + "carrito.php"
      return  this.http.post(URL, data, {headers: headers});
  }


  getCarrito(){
    let token:string  = this._authService.getToken();
    let headers= null;
    if(token){
      headers = new HttpHeaders({'token': token})
    }else{
      headers = new HttpHeaders({'token': ''})

    }
    let URL = this.url + "getCarrito.php"
    return this.http.post(URL, "", {headers: headers});
  }

  updateCarrito(data: any){
    let token:string  = this._authService.getToken();
    let headers= null;;
    if(token){
      headers = new HttpHeaders({'token': token})
    }else{
      headers = new HttpHeaders({'token': ''})

    }
    let URL = this.url + "updateCarrito.php"
    return this.http.post(URL, data, {headers: headers});
  }
  deleteCarrito(data: any){
    let token:string  = this._authService.getToken();
    let headers= null;;
    if(token){
      headers = new HttpHeaders({'token': token})
    }else{
      headers = new HttpHeaders({'token': ''})

    }
    let URL = this.url + "deleteCart.php"
    return this.http.post(URL, data, {headers: headers});
  }
}
