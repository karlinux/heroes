import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  url = environment.URL_SERVICIOS;

  constructor(
    private http: HttpClient,
  ) { }

  getSlider( datos: any ): Observable<any[]> {
    return this.http.post<any>(`${this.url}getSlider.php`, datos);
  }

  getProducto( datos: any ): Observable<any[]> {
    return this.http.post<any>(`${this.url}getProductos.php`, datos);
  }
  getProductoImagenes( datos: any ): Observable<any[]> {
    return this.http.post<any>(`${this.url}getProductosImagenes.php`, datos);
  }

  getNewProductos( datos: any ): Observable<any[]> {
    return this.http.post<any>(`${this.url}getNewProductosImagenes.php`, datos);
  }

}
