import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';
import { environment } from '../../../environments/environment';
import { EcommerceGuestService } from '../ecommerce-guest/_service/ecommerce-guest.service';
import { Router } from '@angular/router';

declare var $:any;
declare function HOMEITTEMPLATE([]):any;
declare function ModelaProductDetail():any;
declare function alertDanger([]): any;
declare function alertSuccess([]): any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  url = environment.URL_SERVICIOS;
  title = 'ecommerce';

  sliders: any = [];
  categorias: any = [];
  bestProductos: any = [];
  exploreProductos: any = [];
  exploreProductos2: any = [];
  newProductos2: any = [];
  product_selected:any = null;
  ventaflash:any = null;

  constructor(
    public homeService: HomeService,
    public ecommerce_guest: EcommerceGuestService,
    public router: Router,
  ){

  }

  ngOnInit(): void{

      this.getSliders(  );
      this.getCategorias(  );
      this.getProductos(  );
      this.getExploreProductos(  );
      this.newProductos( );
  }

  getSliders(  ) {

    let catego = {
      "tabla": 'producto',
      "tipo": 3,
    }

    this.homeService.getSlider( catego ).subscribe(
      data => {
        this.sliders = data;

        setTimeout(() => {
          if (typeof HOMEITTEMPLATE !== 'undefined') {
            HOMEITTEMPLATE($);
          }
        }, 100)
      }
      );
  }

  getCategorias(  ) {

    let catego = {
      "tabla": 'categories',
      "tipo": 2,
    }

    this.homeService.getProducto( catego ).subscribe(
      data => {
        this.categorias = data;
      }
      );
  }
  newProductos(  ) {

    let bestProducto = {
      "tabla": 'producto',
      "tipo": 3,
    }

    this.homeService.getNewProductos( bestProducto ).subscribe(
      data => {
        this.newProductos2 = data;

      }
      );
  }
  getProductos(  ) {

    let bestProducto = {
      "tabla": 'producto',
      "tipo": 3,
    }

    this.homeService.getProductoImagenes( bestProducto ).subscribe(
      data => {
        this.bestProductos = data;

      }
      );
  }
  getExploreProductos(  ) {

    let bestProducto = {
      "tabla": 'producto',
      "tipo": 3,
    }

    this.homeService.getProductoImagenes( bestProducto ).subscribe(
      data => {
        this.exploreProductos = data;
      }
      );
      this.homeService.getProductoImagenes( bestProducto ).subscribe(
        data => {
          this.exploreProductos2 = data;
        }
        );
  }

  openModal(bestProducto: any){
    this.product_selected = null;

    setTimeout(()=>{
      this.product_selected = bestProducto
      setTimeout(() =>{
        ModelaProductDetail();
      }, 50);

    },100)

  }
  browserBy( category: any){

    if(category){
      console.log(category)
    }else{
      console.log("Hola")
    }

  }

  addCart( producto: any, cantidad: number ){

    //let cantidad = $("#qty-cart").val()
    console.log(this.ecommerce_guest._authService.IsAutenticated())
    if(!this.ecommerce_guest._authService.IsAutenticated()){
      this.router.navigateByUrl('/auth/login');

      alertDanger("NECESITA INICIAR SESIÓN ANTES DE AGREGAR AL CARRITO DE COMPRAS");

      return;
    }
    if( cantidad == 0){
      alert("NECESITA AGREGAR UNA CANTIDAD MAYOR A 0 DEL PRODUCTO PARA EL CARRITO")
      return;
    }

    let data = {
      "product_id": producto.id,
      "type_discount": null,
      "discount": 0,
      "cantidad": cantidad,
      "variedad_id": 0,
      "code_cupon": null,
      "code_discount": null,
      "price_unitario": producto.precio_dolar,
      "subtotal": producto.precio_dolar * cantidad,
      "total": producto.precio_dolar * cantidad,
      "tabla": "car_shop",
    }
    console.log(data)

    this.ecommerce_guest.registerCart( data ).subscribe( (resp: any) =>{
      //localStorage.clear
      console.log(resp)
      if(resp){
        if(!resp.token){
          alertDanger("NECESITA INICIAR SESIÓN ANTES DE AGREGAR AL CARRITO DE COMPRAS");
          this.router.navigateByUrl('/auth/login');
        }else{
          this.ecommerce_guest.changeCart(resp.cart);
          alertSuccess("PRODUCTO AGREGADO AL CARRITO " +producto.titulo);
        }
      }else{
        alertDanger("NECESITA INICIAR SESIÓN ANTES DE AGREGAR AL CARRITO DE COMPRAS");
        this.router.navigateByUrl('/auth/login');
      }


    });
  }
}
