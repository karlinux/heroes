import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EcommerceGuestService } from '../../modules/ecommerce-guest/_service/ecommerce-guest.service';
import { HomeService } from 'src/app/modules/home/home.service';
import { environment } from 'src/environments/environment';
import { JwtService } from '../../modules/auth-profile/_services/jwt.service';

declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  url = environment.URL_SERVICIOS;
  listCart:any = [];
  totalCart: any = 0;
  categorias: any = [];
  nombre: string = '';
  user = null;
  constructor(
    public router: Router,
    public cartService: EcommerceGuestService,
    public homeService: HomeService,
    public _jwtService: JwtService,

  ){

  }

  ngOnInit(){
    this.cartService.currentDataCart$.subscribe((resp: any) =>{
       this.listCart = resp;
       this.totalCart = this.listCart.reduce((sum:any, item:any) => Number(sum) + Number(item.total), 0);
       this.totalCart = this.naiveRound(this.totalCart, 2)
    })
    this. getProductos( );
    this.getCategorias(  );

    if ( localStorage.getItem('token') !== null) {
      this.nombre = this._jwtService.decodeToken(this.getToken()).profile.nombre
      this.user = this._jwtService.decodeToken(this.getToken()).id
    }
  }

   naiveRound(num: any, decimalPlaces = 0) {
    var p = Math.pow(10, decimalPlaces);
    return Math.round(num * p) / p;
  }
  getProductos(  ) {

    this.cartService.getCarrito(  ).subscribe((data:any) => {
        data.forEach((cart:any) => {
          this.cartService.changeCart(cart);
        });

      }
      );
  }

  getCategorias(  ) {

    let catego = {
      "tabla": 'categories',
      "tipo": 2,
    }

    this.homeService.getProducto( catego ).subscribe(
      data => {
        this.categorias = data;
      }
      );
  }

  isHome(){
    return this.router.url == "" || this.router.url == "/" ? true : false
  }
  browserBy( category: any){

    console.log(category)

  }
  getToken(): any{

    if (typeof localStorage !== 'undefined') {
        return localStorage.getItem('token');
    }
  }
  logout(){
    localStorage.clear();
    location.reload()
  }

  removeCart(cart: any){
    let data = {
      "id": cart._id,
      "tabla": "car_shop"
    }

 this.cartService.deleteCarrito( data ).subscribe( (resp: any) => {
      console.log(resp)
      this.cartService.removeItemCart( cart );
    })
  }
}
