import { Component, OnInit } from '@angular/core';

declare var $:any;
declare function usuarioMenu([]):any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  ngOnInit(){
    setTimeout(() => {
      if (typeof usuarioMenu !== 'undefined') {
        usuarioMenu($);
      }
    }, 10)
  }

}
