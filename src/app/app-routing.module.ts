import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './modules/home/home.component';
import { AuthGuard } from './modules/auth-profile/_services/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import("./modules/home/home.module").then( m => m.HomeModule)
  },
  {
    path: '',
    loadChildren: () => import("./modules/ecommerce-guest/ecommerce-guest.module").then( m => m.EcommerceGuestModule)
  },
  {
    path: '',
    loadChildren: () => import('./modules/ecommerce-auth/ecommerce-auth.module').then( m => m.EcommerceAuthModule ),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'auth',
    loadChildren: () => import("./modules/auth-profile/auth-profile.module").then( m => m.AuthProfileModule)
  },
  {
    path: '**',
    redirectTo: 'auth'
  },

]


@NgModule({
  imports: [
    RouterModule.forRoot(routes,{useHash: true})
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
